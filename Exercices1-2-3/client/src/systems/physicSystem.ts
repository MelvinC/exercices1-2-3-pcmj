import { ColliderComponent } from "../components/colliderComponent";
import { PlayerComponent } from "../components/playerComponent";
import { Scene } from "../scene";
import { ISystem } from "./system";
import { QuadTree } from "../components/QuadTree/quadTree";
import * as GraphicsAPI from "../graphicsAPI";
import { Rectangle } from "../components/rectangle";

// # Classe *PhysicSystem*
// Représente le système permettant de détecter les collisions
export class PhysicSystem implements ISystem {
  // Méthode *iterate*
  // Appelée à chaque tour de la boucle de jeu
  public iterate(dT: number) {
    const colliders: ColliderComponent[] = [];

    for (const e of Scene.current.entities()) {
      for (const comp of e.components) {
        if (comp instanceof ColliderComponent && comp.enabled) {
          colliders.push(comp);
        }
      }
    }

    const quadTree = new QuadTree(colliders, new Rectangle({xMin:0, yMin:0, xMax:GraphicsAPI.canvas.width, yMax:GraphicsAPI.canvas.height}));
    const finalNodes = quadTree.getFinalNodes(quadTree.getRoot());
    

    for (var i = 0; i<finalNodes.length; i++) {
      const comp=finalNodes[i].getComponents();
      for (var j = 0; j<comp.length; j++){
        if(!comp[j].enabled || !comp[j].owner.active) continue;
        for (var k = 0 ; k<comp.length; k++){
          if(!comp[k].enabled || !comp[k].owner.active) continue;
          if(j==k) continue;
          //Flag & Mask verification
          if((comp[j].flag & comp[k].mask) > 0){
            if(comp[j].area.intersectsWith(comp[k].area)){
              if (comp[j].handler) {
                comp[j].handler!.onCollision(comp[k]);
              }
              if (comp[k].handler) {
                comp[k].handler!.onCollision(comp[j]);
              }
            }
          }
        }
      }
    }
  }
}
