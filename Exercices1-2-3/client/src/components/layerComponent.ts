import { IEntity } from "../entity";
import { IDisplayComponent } from "../systems/displaySystem";
import { Component } from "./component";
import { SpriteComponent } from "./spriteComponent";
import * as GraphicsAPI from "../graphicsAPI";
import { TextureComponent } from "./textureComponent";
import { GlobalConfig } from "../main";


// # Classe *LayerComponent*
// Ce composant représente un ensemble de sprites qui
// doivent normalement être considérées comme étant sur un
// même plan.
export class LayerComponent extends Component<object> implements IDisplayComponent {
  private vertexBuffer!:WebGLBuffer;
  private vertices!:Float32Array;
  private indexBuffer!:WebGLBuffer;
  // ## Méthode *display*
  // La méthode *display* est appelée une fois par itération
  // de la boucle de jeu.
  public display(dT: number) {
    const layerSprites = this.listSprites();
    if (layerSprites.length === 0) {
      return;
    }
    const spriteSheet = layerSprites[0].spriteSheet;

    var GL = GraphicsAPI.context;

    this.vertexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexBuffer);
    this.vertices = new Float32Array(layerSprites.length*4*TextureComponent.vertexSize)
    
    for(var i = 0; i<layerSprites.length; i++){
      var sprite = layerSprites[i];
      this.vertices.set(sprite.getVertices(),i*4*TextureComponent.vertexSize);
      //console.log(sprite.getVertices());
      //console.log(this.vertices);
    }
    GL.bufferData(GL.ARRAY_BUFFER, this.vertices, GL.DYNAMIC_DRAW);

    this.indexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    var indices = new Uint16Array(6*layerSprites.length);
    var j = 0;

    for(var i = 0; i<6*layerSprites.length; i+=6){
      //var sprite = layerSprites[i];
      indices[i]=j;
      indices[i+1]=j+1;
      indices[i+2]=j+2;
      indices[i+3]=j+2;
      indices[i+4]=j+3;
      indices[i+5]=j;
      //GL.bufferSubData(GL.ELEMENT_ARRAY_BUFFER, i*6, new Uint16Array([j,j+1,j+2,j+2,j+3,j]));
      j+=4;
    }
    GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, indices, GL.DYNAMIC_DRAW);
    //console.log(indices);
    GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexBuffer);
    GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    spriteSheet.bind();
    GL.drawElements(GL.TRIANGLES, 6*layerSprites.length, GL.UNSIGNED_SHORT, 0);
    spriteSheet.unbind();
        
  }

  // ## Fonction *listSprites*
  // Cette fonction retourne une liste comportant l'ensemble
  // des sprites de l'objet courant et de ses enfants.
  private listSprites() {
    const sprites: SpriteComponent[] = [];

    const queue: IEntity[] = [this.owner];
    while (queue.length > 0) {
      const node = queue.shift() as IEntity;
      for (const child of node.children) {
        if (child.active) {
          queue.push(child);
        }
      }

      for (const comp of node.components) {
        if (comp instanceof SpriteComponent && comp.enabled) {
          sprites.push(comp);
        }
      }
    }

    return sprites;
  }
}
