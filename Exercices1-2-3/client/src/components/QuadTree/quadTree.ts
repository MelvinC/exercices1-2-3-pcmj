import { ColliderComponent } from "../colliderComponent";
import { Rectangle } from "../rectangle";
import { QuadNode } from "./quadNode";

export class QuadTree {
    private root!:QuadNode;
    
    constructor(components:ColliderComponent[], rect:Rectangle){
        this.root = new QuadNode(rect, 0, components);
    }

    public getFinalNodes(node:QuadNode):QuadNode[]{
        var finalNodes:Array<QuadNode>=[];
        if(node.getChildren().length > 0){
            for(var i = 0 ; i < node.getChildren().length ; i++){
                finalNodes = finalNodes.concat(this.getFinalNodes(node.getChildren()[i]));
            }
        }
        else{
            finalNodes.push(node);
        }
        return finalNodes;
    }

    public getRoot():QuadNode{
        return this.root;
    }
}